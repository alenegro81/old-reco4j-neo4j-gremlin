/*
 * GremlinGraphLoadingUtil.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.reco4j.graph.neo4j.gremlin;

import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.TransactionalGraph;
import com.tinkerpop.gremlin.groovy.Gremlin;
import org.neo4j.graphdb.GraphDatabaseService;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph
/**
 *
 * @author ale
 */
class GremlinGraphLoadingUtil10M
{
  static 
  { 
    Gremlin.load()
  }
  
  public static Map<Vertex, Integer> eigenvectorRank(Graph g) 
  {  
    Map<Vertex,Integer> m = [:]; 
    int c = 0;
    g.V.out.groupCount(m).loop(2) {c++ < 1000}.iterate();
    return m;
  }
  
  public static loadMovieLensDataSet(GraphDatabaseService graphDB, String basePath)
  {
    Neo4jGraph g = new Neo4jGraph(graphDB);
    g.dropIndex("idx_movie");
    g.dropIndex("idx_user");
    g.dropIndex("idx_node_type");
    
    Index<Vertex> movieIndex = g.createIndex("idx_movie", Vertex.class);
    Index<Vertex> userIndex = g.createIndex("idx_user", Vertex.class);
    Index<Vertex> nodeTypeIndex = g.createIndex("idx_node_type", Vertex.class);
    
    //loadUserData(g, basePath + "/u.user");
    loadMovieData(g, basePath + "/movies.dat");
    loadRatingData(g, basePath + "/ratings.dat");
  }
  
  public static loadMovieData(Graph g, String datasource)
  {
    System.out.println("loadMovieData ...");
    Index<Vertex> nodeTypeIndex = g.getIndex("idx_node_type", Vertex.class);
    Index<Vertex> movieIndex = g.getIndex("idx_movie", Vertex.class);
    new File(datasource).eachLine {def line ->
      def components = line.split('\\::');
      def movieVertex = g.addVertex(['type':'Movie', 'movieId':components[0].toInteger(), 'title':components[1]]);
      movieIndex.put("movieId", components[0].toInteger(), movieVertex);
      nodeTypeIndex.put("type", "Movie", movieVertex); 
    }
    System.out.println("... fine!");
    g.stopTransaction(TransactionalGraph.Conclusion.SUCCESS);
  }
  public static loadRatingData(Graph g, String datasource)
  {
    int commitInterval = 1;
    System.out.println("loadRatingData ...");
    Index<Vertex> movieIndex = g.getIndex("idx_movie", Vertex.class);
    Index<Vertex> userIndex = g.getIndex("idx_user", Vertex.class);
    Index<Vertex> nodeTypeIndex = g.getIndex("idx_node_type", Vertex.class);
    new File(datasource).eachLine {def line ->
      def components = line.split('\\::');
      Iterable<Vertex> movieResults = movieIndex.get("movieId", components[1].toInteger());
      Iterable<Vertex> userResults = userIndex.get("userId", components[0].toInteger());
      Vertex user;
      def iterator = userResults.iterator();
      if (iterator.hasNext())
       user =  iterator.next();
      else
      {
        def userVertex = g.addVertex(['type':'User', 'userId':components[0].toInteger()]);
        userIndex.put("userId", components[0].toInteger(), userVertex);
        nodeTypeIndex.put("type", "User", userVertex);
        user = userVertex;
        g.stopTransaction(TransactionalGraph.Conclusion.SUCCESS);
      }
      Vertex movie = movieResults.iterator().next();
      
      def ratedEdge = g.addEdge(user, movie, 'rated');
      ratedEdge.setProperty('RankValue', components[2].toFloat());
      if (commitInterval++ % 200 == 0)
      {
        g.stopTransaction(TransactionalGraph.Conclusion.SUCCESS);
        commitInterval = 1;
      }
    }
    g.stopTransaction(TransactionalGraph.Conclusion.SUCCESS);
    System.out.println("... fine!");
    
  }
  
  public static loadRatingTestData(Graph g, String datasource)
  {
    int commitInterval = 1;
    System.out.println("loadRatingTestData ...");
    Index<Vertex> movieIndex = g.getIndex("idx_movie", Vertex.class);
    Index<Vertex> userIndex = g.getIndex("idx_user", Vertex.class);
    new File(datasource).eachLine {def line ->
      def components = line.split('\\t');
      Iterable<Vertex> movieResults = movieIndex.get("movieId", components[1].toInteger());
      Iterable<Vertex> userResults = userIndex.get("userId", components[0].toInteger());
      def ratedEdge = g.addEdge(userResults.iterator().next(), movieResults.iterator().next(), 'ratedTest');
      ratedEdge.setProperty('RankValue', components[2].toInteger());
      if (commitInterval++ % 200 == 0)
      {
        g.stopTransaction(TransactionalGraph.Conclusion.SUCCESS);
        commitInterval = 1;
      }
    }
    g.stopTransaction(TransactionalGraph.Conclusion.SUCCESS);
    System.out.println("... fine!");
    
  }

}

