/*
 * RecommenderDataSetMovieLensImport.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.gremlin;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.EmbeddedGraphDatabase;

/**
 *
 * @author ale
 */
public class RecommenderDataSetMovieLensImport
{
  private static Logger logger = Logger.getLogger(RecommenderDataSetMovieLensImport.class);

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {
    Properties properties = loadProperties();
    
    Map<String, String> config = new HashMap<String, String>();
    config.put("use_memory_mapped_buffers", "true");
    config.put("neostore.nodestore.db.mapped_memory", "800M");
    config.put("neostore.relationshipstore.db.mapped_memory", "800M");
    config.put("neostore.propertystore.db.mapped_memory", "500M");
    config.put("neostore.propertystore.db.index.mapped_memory", "500M");
    config.put("neostore.propertystore.db.index.keys.mapped_memory", "100M");
    config.put("neostore.propertystore.db.strings.mapped_memory", "500M");
    config.put("neostore.propertystore.db.arrays.mapped_memory", "500M");
    GraphDatabaseService graphDB = (EmbeddedGraphDatabase) new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(properties.getProperty("dbPath")).
      setConfig(config).
      newGraphDatabase();
    registerShutdownHook(graphDB);


    //GremlinGraphLoadingUtil.loadMovieLensDataSet(graphDB, properties.getProperty("movieLensBasePath"));
    GremlinGraphLoadingUtil10M.loadMovieLensDataSet(graphDB, properties.getProperty("movieLensBasePath"));
  }

  private static Properties loadProperties()
  {
    // TODO code application logic here
    Properties properties = new Properties();
    try
    {
      properties.load(RecommenderDataSetMovieLensImport.class.getResourceAsStream("init.properties"));
    }
    catch (IOException ex)
    {
      logger.error("Error while loading properties", ex);
    }
    return properties;
  }

  private static void registerShutdownHook(final GraphDatabaseService graphDb)
  {
    Runtime.getRuntime().addShutdownHook(new Thread()
    {
      @Override
      public void run()
      {
        System.out.println("Shuting down neo4j db instance ...");
        graphDb.shutdown();
        System.out.println("... done!");
      }
    });
  }
}
